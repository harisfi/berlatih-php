@extends('master')
@push('title')
    Cast
@endpush
@section('content')
    <a href="/cast/create" class="btn btn-primary">Tambah</a>
    <table class="table table-hover table-striped table-bordered my-3">
        <thead class="table-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key=>$value)
                <tr>
                    <td>{{ $key + 1 }}</th>
                    <td>{{ $value->nama }}</td>
                    <td>{{ $value->umur }}</td>
                    <td>{{ $value->bio }}</td>
                    <td class="d-flex ">
                        <a href="/cast/{{ $value->id }}" class="btn btn-info btn-sm m-1">Show</a>
                        <a href="/cast/{{ $value->id }}/edit" class="btn btn-primary btn-sm m-1">Edit</a>
                        <form action="/cast/{{ $value->id }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger btn-sm m-1" value="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="5">No data</td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection
