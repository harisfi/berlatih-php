@extends('master')
@push('title')
    Edit Cast
@endpush
@section('content')
    <div>
        <h2>Edit Cast {{ $cast->id }}</h2>
        <form action="/cast/{{ $cast->id }}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group my-2">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama" value="{{ $cast->nama }}" id="nama"
                    placeholder="Masukkan nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group my-2">
                <label for="umur">Umur</label>
                <input type="text" class="form-control" name="umur" value="{{ $cast->umur }}" id="umur"
                    placeholder="Masukkan umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group my-2">
                <label for="bio">Bio</label>
                <input type="text" class="form-control" name="bio" value="{{ $cast->bio }}" id="bio"
                    placeholder="Masukkan bio">
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary my-2">Simpan</button>
            <a href="/cast" class="btn btn-warning m-2">Batal</a>
        </form>
    </div>
@endsection
