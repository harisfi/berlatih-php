@extends('master')
@push('title')
    Show Cast
@endpush
@section('content')
    <h2>Show Cast {{$cast->id}}</h2>
    <p>Nama: {{$cast->nama}}</p>
    <p>Umur: {{$cast->umur}}</p>
    <p>Bio: {{$cast->bio}}</p>
    <a href="/cast" class="btn btn-warning my-2">Kembali</a>
@endsection