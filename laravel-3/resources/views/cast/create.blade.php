@extends('master')
@push('title')
    Tambah Cast
@endpush
@section('content')
    <div>
        <h2>Tambah Data</h2>
        <form action="/cast" method="POST">
            @csrf
            <div class="form-group my-2">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group my-2">
                <label for="umur">Umur</label>
                <input type="number" class="form-control" name="umur" id="umur" placeholder="Masukkan umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group my-2">
                <label for="bio">Bio</label>
                <input type="text" class="form-control" name="bio" id="bio" placeholder="Masukkan bio">
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary my-2">Tambah</button>
            <a href="/cast" class="btn btn-warning m-2">Batal</a>
        </form>
    </div>
@endsection
