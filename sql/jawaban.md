# Jawaban Soal SQL

## Soal 1: Membuat Database
```sql
CREATE DATABASE myshop;
```

## Soal 2: Membuat Table di Dalam Database
```sql
CREATE TABLE users (
  id INT NOT NULL AUTO_INCREMENT, 
  name VARCHAR(255) NOT NULL, 
  email VARCHAR(255) NOT NULL, 
  password VARCHAR(255) NOT NULL, 
  PRIMARY KEY (id)
);

CREATE TABLE categories (
  id INT NOT NULL AUTO_INCREMENT, 
  name VARCHAR(255) NOT NULL, 
  PRIMARY KEY (id)
);

CREATE TABLE items (
  id INT NOT NULL AUTO_INCREMENT, 
  name VARCHAR(255) NOT NULL, 
  description VARCHAR(255) NOT NULL, 
  price INT NOT NULL, 
  stock INT NOT NULL, 
  category_id INT NOT NULL, 
  PRIMARY KEY (id), 
  FOREIGN KEY (category_id) REFERENCES categories(id)
);
```

## Soal 3: Memasukkan Data pada Table
```sql
INSERT INTO users (id, name, email, password) 
VALUES 
  (
    NULL, 'John Doe', 'john@doe.com', 
    'john123'
  ), 
  (
    NULL, 'Jane Doe', 'jane@doe.com', 
    'jenita123'
  );

INSERT INTO categories (id, name) 
VALUES 
  (NULL, 'gadget'), 
  (NULL, 'cloth'), 
  (NULL, 'men'), 
  (NULL, 'women'), 
  (NULL, 'branded');

INSERT INTO items (
  id, name, description, price, stock, 
  category_id
) 
VALUES 
  (
    NULL, 'Sumsang b50', 'hape keren dari merek sumsang', 
    '4000000', '100', '1'
  ), 
  (
    NULL, 'Uniklooh', 'baju keren dari brand ternama', 
    '500000', '50', '2'
  ), 
  (
    NULL, 'IMHO Watch', 'jam tangan anak yang jujur banget', 
    '2000000', '10', '1'
  );

```

## Soal 4: Mengambil Data dari Database

a. Mengambil data users
```sql
SELECT id, name, email FROM users;
```

b. Mengambil data items
```sql
SELECT * FROM items WHERE price > 1000000;
```
```sql
SELECT * FROM items WHERE name LIKE '%uniklo%';
```

c. Menampilkan data items join dengan kategori
```sql
SELECT i.*, c.name "kategori" FROM items i INNER JOIN categories c ON i.category_id = c.id;
```

## Soal 5: Mengubah Data dari Database
```sql
UPDATE items SET price = '2500000' WHERE id = 1;
```