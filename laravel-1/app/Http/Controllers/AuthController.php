<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index()
    {
        return view('register');
    }

    public function create(Request $request)
    {
        $nama = $request->first_name . ' ' . $request->last_name;
        return view('welcome', ['nama' => $nama]);
    }
}
