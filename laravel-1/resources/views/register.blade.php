<!DOCTYPE html>
<html>
<head>
    @include('partials.head')
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf

        <label for="first_name">First name:</label><br><br>
        <input type="text" name="first_name" required><br><br>

        <label for="last_name">Last name:</label><br><br>
        <input type="text" name="last_name" required><br><br>

        <label for="gender">Gender:</label><br><br>
        <input type="radio" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio" name="gender" value="rother">
        <label for="rother">Other</label><br><br>

        <label for="nationality">Nationality:</label><br><br>
        <select name="nationality">
            <option value="1">Indonesian</option>
            <option value="2">Singaporean</option>
            <option value="3">Malaysian</option>
            <option value="4">Australian</option>
        </select><br><br>

        <label for="language">Language Spoken:</label><br><br>
        <input type="checkbox" name="language" value="id">
        <label for="id">Bahasa Indonesia</label><br>
        <input type="checkbox" name="language" value="en">
        <label for="en">English</label><br>
        <input type="checkbox" name="language" value="cother">
        <label for="cother">Other</label><br><br>

        <label for="bio">Bio:</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>
        
        <button type="submit">Sign Up</button>
    </form>
</body>
</html>