<?php
require("./animal.php");
require("./ape.php");
require("./frog.php");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Latihan OOP</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
</head>
<body>
    <?php
    $sheep = new Animal("shaun");

    echo "Name: " . $sheep->name . "<br>"; // "shaun"
    echo "legs: " . $sheep->legs . "<br>"; // 4
    echo "cold blooded: " . $sheep->cold_blooded . "<br><br>"; // "no"

    $kodok = new Frog("buduk");

    echo "Name: " . $kodok->name . "<br>"; // "buduk"
    echo "legs: " . $kodok->legs . "<br>"; // 4
    echo "cold blooded: " . $kodok->cold_blooded . "<br>"; // "no"
    echo "Jump: " . $kodok->jump() . "<br><br>"; // "hop hop"

    $sungokong = new Ape("kera sakti");

    echo "Name: " . $sungokong->name . "<br>"; // "kera sakti"
    echo "legs: " . $sungokong->legs . "<br>"; // 2
    echo "cold blooded: " . $sungokong->cold_blooded . "<br>"; // "no"
    echo "Yell: " . $sungokong->yell() . "<br>"; // "Auooo"
    ?>
</body>
</html>